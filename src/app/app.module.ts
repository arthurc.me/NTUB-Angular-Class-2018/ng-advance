import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ShareModule } from './share/share.module';
import { NormalSubComponent } from './normal-sub/normal-sub.component';
import { NormalSub1Component } from './normal-sub1/normal-sub1.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NormalSubComponent,
    NormalSub1Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ShareModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
