import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NormalSub1Component } from './normal-sub1.component';

describe('NormalSub1Component', () => {
  let component: NormalSub1Component;
  let fixture: ComponentFixture<NormalSub1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NormalSub1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NormalSub1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
