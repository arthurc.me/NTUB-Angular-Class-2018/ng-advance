import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { NormalSubComponent } from './normal-sub/normal-sub.component';
import { NormalSub1Component } from './normal-sub1/normal-sub1.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  {
    path: 'home', component: HomeComponent, children: [
      { path: '', redirectTo: 'sub', pathMatch: 'full'},
      { path: 'sub', component: NormalSubComponent },
      { path: 'sub1', component: NormalSub1Component },
    ]
  },
  { path: 'login', loadChildren: './login/login.module#LoginModule' },
  { path: 'post', loadChildren: './post/post.module#PostModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
