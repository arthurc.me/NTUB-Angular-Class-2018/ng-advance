import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LazySub1Component } from './lazy-sub1.component';

describe('LazySub1Component', () => {
  let component: LazySub1Component;
  let fixture: ComponentFixture<LazySub1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LazySub1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LazySub1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
