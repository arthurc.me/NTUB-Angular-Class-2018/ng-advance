import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { LazySubComponent } from './lazy-sub/lazy-sub.component';
import { LazySub1Component } from './lazy-sub1/lazy-sub1.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children: [
      { path: '', redirectTo: 'sub', pathMatch: 'full' },
      { path: 'sub', component: LazySubComponent },
      { path: 'sub1', component: LazySub1Component }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LoginRoutingModule { }
