import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { LazySubComponent } from './lazy-sub/lazy-sub.component';
import { LazySub1Component } from './lazy-sub1/lazy-sub1.component';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule
  ],
  declarations: [
    LoginComponent,
    LazySubComponent,
    LazySub1Component
  ]
})
export class LoginModule { }
